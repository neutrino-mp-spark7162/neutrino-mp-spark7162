#!/bin/sh

#### 3G modem init. by Lexandr0s 04.04.2014. http://gisclub.tv



if [ -f /var/modem/autodial.conf ]
then
	read AUTODIAL < /var/modem/autodial.conf
fi

if [ -f "/tmp/modemctrl.log" ]
then
	rm -f /tmp/modemctrl.log
fi

ls /dev | grep ttyUSB > /dev/null
if [ $? = 0 ]
then
	echo "The modem already is installed!" > /tmp/modemctrl.log
	exit 0
fi
	
succ=0
count=5

while [ $succ -eq 0 ]
do
	count=$(( $count - 1 ))
	usbcount=$( lsusb | wc -l )

	n=0
	flag=0
	while [ $n -ne $usbcount ]
	do
	
		n=$(( $n +1 ))
		#echo $n
		id=$( lsusb | sed -n "$n"p | awk '{print $6}' )
		C="/share/usb_modeswitch/$id"
		if [ -f "$C" ]
		then
			flag=1
			echo $n $id This modem! >> /tmp/modemctrl.log
			VENDORID=$( echo $id | awk -F: '{print($1)}' )
			PRODUCTID=$( echo $id | awk -F: '{print($2)}' )
			usb_modeswitch -v $VENDORID -p $PRODUCTID -c $C > /dev/null
			newusbcount=0
			while [ $newusbcount -ne $usbcount ]
			do
				newusbcount=$( lsusb | wc -l )
				sleep 1
			done
			m=0
			while [ $m -ne $usbcount ]
			do
				m=$(( $m +1 ))
				newid=$( lsusb | sed -n "$m"p | awk '{print $6}' )
				modem_name=$( sed -n '/'$newid'/p' /var/modem/modem.list | awk -F: '{print $4}' )
				if [ ! -z "$modem_name" ]
				then
					echo "Found modem $modem_name" >> /tmp/modemctrl.log
					NEWVENDORID=$( echo $newid | awk -F: '{print($1)}' )
					NEWPRODUCTID=$( echo $newid | awk -F: '{print($2)}' )
					lsmod | grep usbserial > /dev/null
					if [ $? = 0 ]
					then
						echo Uninstall module >> /tmp/modemctrl.log
						rmmod -f usbserial
					fi
				
					echo Install module >> /tmp/modemctrl.log
					modprobe -q usbserial vendor=0x$NEWVENDORID product=0x$NEWPRODUCTID
					sleep 10
					ls /dev | grep ttyUSB > /dev/null
					if [ $? = 0 ]
					then
						echo "The modem successfull installed!" >> /tmp/modemctrl.log
						succ=1
					
						if [ $AUTODIAL -eq 1 ]
						then
							echo "Autodial ON. Go dialing!"	>> /tmp/modemctrl.log
							exec /var/modem/modemdial.sh start
						fi
					else
						if [ $count -eq 0 ]
						then
							succ=1
							echo Try count end. Unsuccessfull installed >> /tmp/modemctrl.log
						else
							echo The modem installed unsuccessfull. Try again! >> /tmp/modemctrl.log
						fi
					fi
					m=$usbcount
				fi
			done
		n=$usbcount
		fi
	done

	if [ $n -eq $usbcount ]
	then
		succ=1
	fi

done


if [ $flag -ne 1 ]
then
	echo No modem found >> /tmp/modemctrl.log
fi




exit 0
