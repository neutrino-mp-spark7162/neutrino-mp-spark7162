#!/bin/sh

pppflag=1
pppcount=100
while [ $pppflag -eq 1 ]
do
       ifconfig | grep ppp > /dev/null
       pppflag=$?
       pppcount=$(( $pppcount - 1 ))
#       echo "$pppcount   $pppflag"
       if [ $pppcount -eq 0 ]
       then
               echo "The maximum time is reached! Check your config! Or try later. Break." >> /tmp/modemdial.log
		pppflag=2
       fi
       sleep 1
done

if [ $pppflag -eq 0 ]
then 
	exit 0
else
	exit 1
fi
