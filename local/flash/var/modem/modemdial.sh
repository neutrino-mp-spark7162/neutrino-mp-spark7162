#!/bin/sh

#### 3G modem dialing. by Lexandr0s 04.04.2014. http://gisclub.tv
#### It is partially taken from a AR-p Enigma2. Thanks to guys from AR-p team!


ls /dev | grep ttyUSB > /dev/null
if [ $? -eq 0 ]
then
	echo "Modem found!" > /tmp/modemdial.log
else
	echo "Modem not found! BreaK." > /tmp/modemdial.log
	exit 1
fi
		
if [ -f /var/modem/modem.conf ]
then
	. /var/modem/modem.conf
	echo "Modem config found!" >> /tmp/modemdial.log
else
	echo "Modem config not found! Break." >> /tmp/modemdial.log
	exit 1
fi



start(){
if [ ! -d /var/lock ]; then
    mkdir -p /var/lock
fi

#exit 0

if [ ! -d /etc/ppp/peers ]; then
    mkdir -p /etc/ppp/peers
fi
echo "ABORT '~'
ABORT 'BUSY'
ABORT 'NO CARRIER'
ABORT 'ERROR'
REPORT 'CONNECT'
'' 'ATZ'
SAY 'Calling WCDMA/UMTS/GPRS'
'' 'AT+CGDCONT=1,\"IP\",\"$APN\"'
'OK' 'ATD$DIALNUMBER'
'CONNECT' ''" > /etc/ppp/peers/0.chat

echo "ABORT '~'
ABORT 'BUSY'
ABORT 'NO CARRIER'
ABORT 'ERROR'
ABORT 'NO DIAL TONE'
ABORT 'NO ANSWER'
ABORT 'DELAYED'
REPORT 'CONNECT'
'' 'ATZ'
SAY 'Calling CDMA/EVDO'
'OK' 'ATDT#777'
'CONNECT' 'ATO'
'' ''" > /etc/ppp/peers/1.chat

echo "debug
/dev/$MODEMPORT
$MODEMSPEED
crtscts
noipdefault
lock
ipcp-accept-local
ipcp-accept-remote
lcp-echo-interval 60
lcp-echo-failure 6
mtu $MODEMMTU
mru $MODEMMRU
usepeerdns
defaultroute
noauth
maxfail 0
holdoff 5
$MODEMPPPDOPTS
nodetach
persist
user $MODEMUSERNAME
password $MODEMPASSWORD
connect \"/sbin/chat -s -S -V -t 60 -f /etc/ppp/peers/$MODEMTYPE.chat 2>/tmp/chat.log\"" > /etc/ppp/peers/dialup

if [ ! -c /dev/ppp ]; then
    mknod /dev/ppp c 108 0
fi
old_route=$( route | grep default | awk '{print $2}' )
echo $old_route > /var/modem/old_route.conf
route del default
echo nameserver 8.8.8.8 > /etc/resolv.conf
echo "Dialing. Please wait!" >> /tmp/modemdial.log
pppd call dialup 2>> /tmp/chat.log &

/var/modem/pppcheck.sh

if [ $? -eq 0 ]
then
	/etc/init.d/cam restart > /dev/null
	echo "Connect successfull! Default route added to route table, Cam restarted. Enjoy!" >> /tmp/modemdial.log
else
	echo "Connect error! Check your modem and config or try later!" >> /tmp/modemdial.log
fi


}

stop(){
echo "Connect stopped" >> /tmp/modemdial.log
killall pppd > /dev/null
if [ -f /var/modem/old_route.conf ]
then
read new_route < /var/modem/old_route.conf
fi

if [ ! -z $new_route ]
then
route add default gw $new_route
fi

/etc/init.d/cam restart > /dev/null

rm -rf /etc/ppp/peers/0.chat
rm -rf /etc/ppp/peers/1.chat
rm -rf /etc/ppp/peers/dialup
rm -rf /etc/ppp/resolv.conf
}



if [ -z "$MODEMMTU" ] || [ "$MODEMMTU" = "auto" ]; then
    MODEMMTU=1492
fi
if [ -z "$MODEMMRU" ] || [ "$MODEMMRU" = "auto" ]; then
    MODEMMRU=1492
fi
if [ -z "$DIALNUMBER" ] || [ "$DIALNUMBER" = "auto" ]; then
    if [ "$MODEMTYPE" = "0" ]; then
	DIALNUMBER="*99#"
    else
	DIALNUMBER="#777"
    fi
fi

if [ "$DEBUG" = "1" ]; then
    echo -e "==================================================\nACTION $1 MODEMPORT $2" >> /tmp/modem.log
else
    rm -rf /tmp/modem.log
fi
if [ "$MODEMPORT" = "auto" ]; then
    LIST=`lsusb | awk '{print $6}'`
    for vidpid in $LIST; do
	PORT=`cat /var/modem/modem.list | grep "$vidpid"|cut -f 3 -d : -s`
	if [ ! -z $PORT ]; then
	    break
	fi
    done
    MODEMPORT="tty$PORT"
    if [ "$DEBUG" = "1" ]; then
	echo "Detected modem port is $MODEMPORT"  >> /tmp/modem.log
    fi
fi

case $1 in

    start)
	start
    ;;

    stop)
	stop
    ;;

    add)
	if [ "$MODEMAUTOSTART" = "1" ] && [ "$MODEMPORT" = "$2" ]; then
	    stop
	    sleep 2
	    start
	fi
    ;;

    remove)
    if [ "$MODEMPORT" = "$2" ]; then
	stop
    fi
    ;;
    restart)
	stop
	sleep 10
	start
    ;;

    *)
    ;;

esac

exit 0
