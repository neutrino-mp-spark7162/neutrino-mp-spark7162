#!/bin/sh

if [ -f /var/modem/old_route.conf ]
	then read new_route < /var/modem/old_route.conf
fi

old_route=$( route | grep default | awk '{print $2}' )



if [ ! -z $new_route ]
then
	old_route=$( route | grep default | awk '{print $2}' )
	if [ "$old_route" != "$new_route" ]
	then
		route add default gw $new_route
		/etc/init.d/cam restart > /dev/null
	fi
fi
