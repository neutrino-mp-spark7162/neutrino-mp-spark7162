#!/bin/sh

#### 3G modem flexmenu routine. by Lexandr0s 04.04.2014. http://gisclub.tv


load(){
. /var/modem/newmodem.conf
read AUTODIAL < /var/modem/newautodial.conf
}

write() {
echo MODEMTYPE=$MODEMTYPE > /var/modem/newmodem.conf
echo MODEMPORT=$MODEMPORT >> /var/modem/newmodem.conf
echo MODEMSPEED=\"$MODEMSPEED\" >> /var/modem/newmodem.conf
echo APN=\"$APN\" >> /var/modem/newmodem.conf
echo MODEMUSERNAME=\"$MODEMUSERNAME\" >> /var/modem/newmodem.conf
echo MODEMPASSWORD=\"$MODEMPASSWORD\" >> /var/modem/newmodem.conf
echo MODEMMTU=$MODEMMTU >> /var/modem/newmodem.conf
echo MODEMMRU=$MODEMMRU >> /var/modem/newmodem.conf
echo MODEMPPPDOPTS=\"$MODEMPPPDOPTS\" >> /var/modem/newmodem.conf
echo DIALNUMBER=$DIALNUMBER >> /var/modem/newmodem.conf
echo MODEMAUTOSTART=$MODEMAUTOSTART >> /var/modem/newmodem.conf
echo DEBUG=$DEBUG >> /var/modem/newmodem.conf
echo SHARE=$SHARE >> /var/modem/newmodem.conf
echo $AUTODIAL > /var/modem/newautodial.conf
}

old_conf () {
. /var/modem/modem.conf
read AUTODIAL < /var/modem/autodial.conf
}


case $1 in
        nAPN)
	OLD_APN=$( sed -n '/&1/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&1/s/:'$OLD_APN'/:'$2'/' /var/tuxbox/config/flex/flex_modem.conf
	load
	APN=$2
	write
        ;;

	nNAME)
        OLD_NAME=$( sed -n '/&2/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&2/s/:'$OLD_NAME'/:'$2'/' /var/tuxbox/config/flex/flex_modem.conf
        load
        MODEMUSERNAME=$2
        write
        ;;

	nPASS)
        OLD_PASS=$( sed -n '/&3/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&3/s/:'$OLD_PASS'/:'$2'/' /var/tuxbox/config/flex/flex_modem.conf
        load
	MODEMPASSWORD=$2
        write
        ;;

	nDIAL)
        OLD_DIAL=$( sed -n '/&4/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&4/s/:'$OLD_DIAL'/:'$2'/' /var/tuxbox/config/flex/flex_modem.conf
        load
        AUTODIAL=$2
        write
        ;;



	save)
	cp -f /var/modem/newmodem.conf /var/modem/modem.conf
	cp -f /var/modem/newautodial.conf /var/modem/autodial.conf
        ;;

	cancel)
	old_conf
	OLD_APN=$( sed -n '/&1/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&1/s/:'$OLD_APN'/:'$APN'/' /var/tuxbox/config/flex/flex_modem.conf
	OLD_NAME=$( sed -n '/&2/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&2/s/:'$OLD_NAME'/:'$MODEMUSERNAME'/' /var/tuxbox/config/flex/flex_modem.conf
	OLD_PASS=$( sed -n '/&3/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&3/s/:'$OLD_PASS'/:'$MODEMPASSWORD'/' /var/tuxbox/config/flex/flex_modem.conf
	OLD_DIAL=$( sed -n '/&4/p' /var/tuxbox/config/flex/flex_modem.conf | awk -F: '{print $2}' |  awk -F, '{print $1}' )
        sed -i '/&4/s/:'$OLD_DIAL'/:'$AUTODIAL'/' /var/tuxbox/config/flex/flex_modem.conf
	write
        ;;


esac

exit 0
